// import logo from './logo.svg';
// import './App.css';
// import Nav from './Nav';
// import AttendeesList from './AttendeesList';
// import LocationForm from './LocationForm';


// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;


// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <div>
//       Number of attendees: {props.attendees.length}
//     </div>
//   );
// }

// export default App;


import {
  BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from "./AttendConferenceForm";
import ConferenceForm from "./ConferenceForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
      <Route index element={<MainPage />} />
          <Route path ="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path ="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path ="attendees" element={<AttendeesList attendees={props.attendees} />}>
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
        </Routes>
        </div>
        </BrowserRouter>
  );
  }

  export default App;






//   const router = createBrowserRouter([
// {
//   path: "locations",
//   children: [
//     {path: "attendees", element: <AttendeesList /> },
//   ]
// },
//   ])
//   return  <RouterProvider router={router} />;
// }
//     <>
//       <Nav />
//       <div className="container">
//         <LocationForm />
//         <AttendeesList attendees={props.attendees} />
//       </div>
//     </>
//   ;







//This is my function /////////////////////////////////
// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }

//   return (
//     <div>
//     <Nav />
//     <div className="container">
//       <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>
//           {props.attendees.map(attendee => {
//             return(
//               <tr key={attendee.href}>
//                 <td>{ attendee.name }</td>
//                 <td>{ attendee.conference }</td>
//               </tr>
//             );
//            })}
//         </tbody>
//       </table>
//     </div>
//     </div>
//   );
// }


// export default App;
//////////////////////////////////////////////////////////////////


// function App(props) {
//   if (!props.attendees || !Array.isArray(props.attendees) || props.attendees.length === 0) {
//     return <div>No attendees to display.</div>;
//   }
//   return (
//     <div>
//       <table>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>
//           {props.attendees.map((attendee, index) => (
//             <tr key={index}>
//               <td>{attendee.name}</td>
//               <td>{attendee.conference || 'N/A'}</td>
//             </tr>
//           ))}
//         </tbody>
//       </table>
//     </div>
//   );
// }
// export default App;
