import React, { useEffect } from 'react';

function LocationForm( ) {
    const [states, setStates] = useState([]);

    const fetchData = () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <p>A location form</p>
  )
}

export default LocationForm;
